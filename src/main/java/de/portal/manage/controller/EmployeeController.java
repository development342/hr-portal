package de.portal.manage.controller;

import de.portal.manage.exception.ResourceNotFoundException;
import de.portal.manage.model.Employee;
import de.portal.manage.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.*;

@RestController
@RequestMapping("/api/")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/hello")
    public String helloMessage(
            @RequestParam(name = "strVal", defaultValue = "Application")
            String strVal) {
        return String.format("Hello %s", strVal);
    }

    @GetMapping("/all-employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @PostMapping("/save-employee")
    public Employee saveEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }


    @GetMapping("/single-employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(
            @PathVariable(value = "id")
            Long employeeId) throws ResourceNotFoundException{

        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "Employee not found for this id::" + employeeId));
        return ResponseEntity.ok().body(employee);
    }

    @PutMapping("/update-employee/{id}")
    public ResponseEntity<Employee> updateEmployee(
            @PathVariable(value = "id")
                    Long employeeId,
            @RequestBody
                    Employee employeeNewData) throws ResourceNotFoundException{

            Employee employeeOldData = employeeRepository.findById(employeeId)
                    .orElseThrow(()->
                            new ResourceNotFoundException(
                                    "Employee Not Found For This Id::" + employeeId));

            employeeOldData.setEmployeeName(employeeNewData.getEmployeeName());
            employeeOldData.setEmployeeFirstName(employeeNewData.getEmployeeFirstName());
            employeeOldData.setEmployeeLastName(employeeNewData.getEmployeeLastName());
            employeeOldData.setEmail(employeeNewData.getEmail());
            employeeOldData.setDateOfBirth(employeeNewData.getDateOfBirth());
            employeeOldData.setBasicSalary(employeeNewData.getBasicSalary());
            employeeOldData.setSkills(employeeNewData.getSkills());
            employeeOldData.setDepartment(employeeNewData.getDepartment());
            employeeOldData.setStatus(employeeNewData.getStatus());

            Employee employeeUpdatedData = employeeRepository.save(employeeOldData);

            return ResponseEntity.ok().body(employeeUpdatedData);
    }

    @PatchMapping("/update-single-value-in-employee/{id}")
    public ResponseEntity<Employee> updateSalary(
            @PathVariable(value = "id")
            Long id,
            @RequestBody
            Map<Object,Object> employeeDataMap) throws ResourceNotFoundException{

            Employee employeeData = employeeRepository.findById(id)
                    .orElseThrow(()->
                            new ResourceNotFoundException(
                                    "Employee data not found for this id::" + id));

            employeeDataMap.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(Employee.class, (String) key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, employeeData, value);
            });
            return ResponseEntity.ok().body(employeeRepository.save(employeeData));
    }


    @DeleteMapping("/delete/employee/{id}")
    public Map<String, Boolean> deleteEmployee(
            @PathVariable(value = "id")
                    Long employeeId) throws ResourceNotFoundException{

            Employee employee = employeeRepository.findById(employeeId)
                    .orElseThrow(()->
                            new ResourceNotFoundException(
                                    "Resource not found for this id::" + employeeId));

            employeeRepository.delete(employee);
            Map<String, Boolean> response = new HashMap<>();
            response.put("deleted", Boolean.TRUE);
            return response;
    }

}
