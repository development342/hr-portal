package de.portal.manage;

import de.portal.manage.exception.ResourceNotFoundException;
import de.portal.manage.model.Employee;
import de.portal.manage.repository.EmployeeRepository;
import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeTests {

    @Autowired
    EmployeeRepository employeeRepository;

    @Test
    @Rollback(false)
    @Order(1)
    public void testSaveEmployee() {
        Employee employee = new Employee(
                "Abraham Lincoln", "Abraham", "Lincoln",
                "01-03-1987", "75000", "abraham.lincoln@test.com",
                "Linux, Windows, DevOps", "Active", "IT"
        );

        Employee savedEmployee = employeeRepository.save(employee);
        Assert.assertNotNull(savedEmployee);
    }

    @Test
    @Order(2)
    public void testFindEmployeeById() throws ResourceNotFoundException{

        List<Employee> allEmployees = employeeRepository.findAll();

        Long id = allEmployees.get(0).getId();

        Employee employee = employeeRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Test Failed for this id::" + id));

        Assert.assertNotNull(employee);
        Assert.assertEquals(employee.getEmployeeName(), allEmployees.get(0).getEmployeeName());

    }

    @Test
    @Order(3)
    @Rollback(false)
    public void testDeleteEmployee() throws ResourceNotFoundException{

        int employeeListSizeBeforeDelete = 0;
        int employeeListSizeAfterDelete = 0;

        List<Employee> allEmployee = employeeRepository.findAll();

        employeeListSizeBeforeDelete = allEmployee.size();

        Long id = -1L;
        for(Employee employee: allEmployee) {
            if(employee.getEmail().equals("abraham.lincoln@test.com")){
                id = new Long(employee.getId());
            }
        }

        Long finalId = id;
        Employee employee = employeeRepository.findById(finalId)
                .orElseThrow(()-> new ResourceNotFoundException("Test failed for this id::" + finalId));

        employeeRepository.delete(employee);

        allEmployee = employeeRepository.findAll();

        employeeListSizeAfterDelete = allEmployee.size();

        Assert.assertFalse("Employee Not Deleted",
                (employeeListSizeBeforeDelete == employeeListSizeAfterDelete ? Boolean.TRUE : Boolean.FALSE));


    }
}
