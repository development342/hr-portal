package de.portal.manage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.portal.manage.controller.EmployeeController;
import de.portal.manage.model.Employee;
import de.portal.manage.repository.EmployeeRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeControllerIntTest {

    @MockBean
    EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private EmployeeController employeeController;

    ObjectMapper objectMapper = new ObjectMapper();
    ObjectWriter objectWriter = objectMapper.writer();

    Employee employee_1 = new Employee("Max Muestarmann", "Max", "Muestarmann"
            , "05-07-1983", "65000","max.muestarmann@test.com", "Java, Python"
            , "Active", "Developer");

    Employee employee_2 = new Employee("Adam Gilchirst", "Adam", "Gilchirst"
            , "03-05-1985", "57000","adam.gilchirst@test.com", "C++, C#"
            , "Parental Leave", "Consultant");

    Employee employee_3 = new Employee("Ricky Ponting", "Ricky", "Ponting"
            , "19-09-1989", "53000","ricky.ponting@test.com", "Linux, Windows"
            , "Sick Leave", "IT");


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(employeeController).build();
    }

    @Test
    public void testHello() throws Exception {
        RequestBuilder requestBuilder = get("/api/hello");
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        Assert.assertEquals("Hello Application", mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testHelloWithStringValue() throws Exception {
        mockMvc.perform(get("/api/hello?strVal=Spring-boot"))
                .andExpect(content().string("Hello Spring-boot"));
    }

    @Test
    public void testGetAllEmployees() throws Exception {
        List<Employee> listOfEmployees = new ArrayList<>(Arrays.asList(employee_1, employee_2, employee_3));
        Mockito.when(employeeRepository.findAll()).thenReturn(listOfEmployees);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/all-employees")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].employeeName", Matchers.is("Ricky Ponting")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].employeeName", Matchers.is("Adam Gilchirst")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeName", Matchers.is("Max Muestarmann")));
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        employee_1.setId(1L);
        Mockito.when(employeeRepository.findById(employee_1.getId())).thenReturn(Optional.ofNullable(employee_1));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/single-employee/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeName", Matchers.is("Max Muestarmann")));
    }

    @Test
    public void testSaveEmployee() throws Exception {

        Employee employee = Employee.builder()
                .id(3L)
                .employeeName("Glen McGrath")
                .employeeFirstName("Glen")
                .employeeLastName("McGrath")
                .dateOfBirth("23-03-1983")
                .basicSalary("63500")
                .email("glen.mcgrath@test.com")
                .skills("Angular, Typscript")
                .status("Active")
                .department("Developer")
                .build();

        Mockito.when(employeeRepository.save(employee)).thenReturn(employee);

        String content = objectWriter.writeValueAsString(employee);

        MockHttpServletRequestBuilder mockHttpServletRequestBuilder = MockMvcRequestBuilders.post("/api/save-employee")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);

        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.employeeName", is("Glen McGrath")));
    }

}
