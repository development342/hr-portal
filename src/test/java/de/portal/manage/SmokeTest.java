package de.portal.manage;

import de.portal.manage.controller.EmployeeController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private EmployeeController employeeController;

    @Test
    public void contextLoad() throws Exception{
        // Small comment for git test
        assertThat(employeeController).isNotNull();
    }
}
