# Sample REST CRUD API application using Java SpringBoot, PostgreSql, JPA, Hibernate
## _Project name : hr-portal_

# Technologies
- Java Springboot 2.6.6
- Java 1.8
- Postgresql 12.10
- Intellij IDE
- Postman
- Maven
- Junit Vintage 5.8.2
- DBeaver-ce
- GitLab

## Features and Endpoints

| Features | Method | Endpoints |
| ------ | ------ |  ------ |
| Save Employee | POST | /api/save-employee |
| Fetch Employee List | GET | /api/all-employees |
| Fetch Employee By id | GET | /api/single-employee/{id} |
| Update Employee's all attributes | PUT | /api/update-employee/{id} |
| Update Employee's single attribute | PATCH | /api/update-single-value-in-employee/{id} |
| Delete Employee | DELETE | /api/delete/employee/{id} |

## Steps to setup
#### 1. Clone the application
```sh
git@gitlab.com:development342/hr-portal.git
```

#### 2. Create Postgresql Database
```sh
create database hrportal
```

#### 3. Change postgresql user name and password as per your installation at application.properties file
```sh
spring.datasource.username=techbal
spring.datasource.password=sec@17
```

#### 4. Run the application
```sh
mvn spring-boot:run
```

#### 5. Sent GET request at postman to this URL
```sh
http://localhost:8081/api/hello
```

#### 6. It should return this message
```sh
Hello Application
```
